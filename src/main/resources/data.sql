INSERT INTO vs_project.project (created_by,created_date,last_modified_date,version,project_description,project_name,project_status) VALUES 
(1,'2021-04-13 13:10:04.0','2021-04-13 13:10:04.0',0,'Project 1 form Test 1','Project 1',1)
,(1,'2021-04-13 13:10:16.0','2021-04-13 13:10:16.0',0,'Project 2 form Test 2','Project 2',1)
,(1,'2021-04-13 13:10:27.0','2021-04-13 13:10:27.0',0,'Project 3 form Test 3','Project 3',1)
,(1,'2021-04-13 13:10:37.0','2021-04-13 13:10:37.0',0,'Project 4 form Test 4','Project 4',1)
,(1,'2021-04-13 13:10:45.0','2021-04-13 13:10:45.0',0,'Project 5 form Test 5','Project 5',1)
;
INSERT INTO vs_project.project_folders (created_by,created_date,last_modified_date,version,project_folder_structure,project_foldre_status) VALUES 
(1,'2021-04-13 13:10:04.0','2021-04-13 13:10:04.0',0,'/documents/input',1)
,(1,'2021-04-13 13:10:04.0','2021-04-13 13:10:04.0',0,'/documents/output',2)
,(1,'2021-04-13 13:10:16.0','2021-04-13 13:10:16.0',0,'/documents/output',2)
,(1,'2021-04-13 13:10:16.0','2021-04-13 13:10:16.0',0,'/documents/input',1)
,(1,'2021-04-13 13:10:27.0','2021-04-13 13:10:27.0',0,'/documents/output',2)
,(1,'2021-04-13 13:10:27.0','2021-04-13 13:10:27.0',0,'/documents/input',1)
,(1,'2021-04-13 13:10:37.0','2021-04-13 13:10:37.0',0,'/documents/output',2)
,(1,'2021-04-13 13:10:37.0','2021-04-13 13:10:37.0',0,'/documents/input',1)
,(1,'2021-04-13 13:10:45.0','2021-04-13 13:10:45.0',0,'/documents/output',2)
,(1,'2021-04-13 13:10:45.0','2021-04-13 13:10:45.0',0,'/documents/input',1)
;
INSERT INTO vs_project.project_project_folders (project_id,project_folders_id) VALUES 
(1,1)
,(1,2)
,(2,3)
,(2,4)
,(3,5)
,(3,6)
,(4,7)
,(4,8)
,(5,9)
,(5,10)
;
