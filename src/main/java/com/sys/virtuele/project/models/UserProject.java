package com.sys.virtuele.project.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sys.virtuele.common.models.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserProject extends BaseDto implements Serializable{
	

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 662315549735289623L;

	private Long id;
	
	private Long projectId;
	private String projectName;

}
