package com.sys.virtuele.project.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sys.virtuele.project.domain.Project;
import com.sys.virtuele.project.repositories.ProjectRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProjectServiceImpl implements ProjectService{

	@Autowired
	ProjectRepository projectRepository;
	
	@Override
	public Project createProject(Project project) {
		return projectRepository.save(project);
	}

	@Override
	public Optional<Project> getProjectDetailsById(Long projectId) {
		
		return projectRepository.findById(projectId);
	}

	@Override
	public List<Project> getAllProjects() {
		return projectRepository.findAll();
	}

	@Override
	public List<Project> getAllProjectsByProjectIds(List<Long> projectIds) {
		
		return projectRepository.findByIdIn(projectIds);
	}

//	@Override
//	public List<Project> getAllProjectsByUserId(Long userId) {
//		
//		
//		log.info(">> User Id : " + userId);
//		return projectRepository.findByUserId(userId);
//	}
//	
}
