package com.sys.virtuele.project.services;

import java.util.List;
import java.util.Optional;

import com.sys.virtuele.project.domain.Project;


/**
 * 
 * @author pnc
 *
 */
public interface ProjectService {
	
	public Project createProject(Project project);
	public Optional<Project> getProjectDetailsById (Long projectId);
	public List<Project> getAllProjects();
	public List<Project> getAllProjectsByProjectIds(List<Long> projectIds);

}
