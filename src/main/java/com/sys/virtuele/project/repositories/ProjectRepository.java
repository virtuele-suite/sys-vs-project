package com.sys.virtuele.project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sys.virtuele.project.domain.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long>{

	//List<Project> findAllByUserId(Long userId);

	List<Project> findByIdIn(List<Long> projectIds);

}
