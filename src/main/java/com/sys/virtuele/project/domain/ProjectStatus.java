package com.sys.virtuele.project.domain;

public enum ProjectStatus {
	INITIATED,WIP,MARKED_FOR_CLOSE,CLOSED,INACTIVE
}
