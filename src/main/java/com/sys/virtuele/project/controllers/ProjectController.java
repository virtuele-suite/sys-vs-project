package com.sys.virtuele.project.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sys.virtuele.project.domain.Project;
import com.sys.virtuele.project.models.UserProject;
import com.sys.virtuele.project.services.ProjectService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/project")
@RestController
public class ProjectController {
	
	@Autowired
	private ProjectService projectService;
	
	@PostMapping
	public ResponseEntity<Project> createProject(@RequestBody Project project) {
		
		
		Project savedProject = projectService.createProject(project);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/api/v1/project/" + savedProject.getId());
		
		log.info("<< Added New User !");
		
		return new ResponseEntity<Project>(headers, HttpStatus.CREATED);
	}
	
	@GetMapping("/{projectId}")
	public ResponseEntity<Project> getProjectDetailsById(@PathVariable("projectId") Long projectId) {
		
		Optional<Project> project = projectService.getProjectDetailsById(projectId);
		
		log.info("<< Added New User !");
		
		return new ResponseEntity<>(project.get(), HttpStatus.CREATED);
	}
	
	
	@GetMapping()
	public ResponseEntity<List<Project>> getAllProjects() {
		
		List<Project> projectList = projectService.getAllProjects();
		
		log.info("<< Added New User !");
		
		return new ResponseEntity<List<Project>>(projectList, HttpStatus.CREATED);
	}
	
	@PostMapping("/fetch/projects")
	public ResponseEntity<List<Project>> getAllProjects(@RequestBody List<UserProject> userProject) {
		
		// Convert List of projectIds to List of Long using Lambda Functions
		List<Long> projectIds = userProject.stream().map(UserProject::getProjectId).collect(Collectors.toList());
		
		List<Project> projectList = projectService.getAllProjectsByProjectIds(projectIds);
		
		log.info("<< Fetching Projects based on Project Ids !");
		
		return new ResponseEntity<List<Project>>(projectList, HttpStatus.CREATED);
	}
	
	
//	@GetMapping("/user/{userId}")
//	public ResponseEntity<List<Project>> getAllProjectsByUserId(@PathVariable("userId") Long userId) {
//		
//		List<Project> projectList = projectService.getAllProjectsByUserId(userId);
//		
//		log.info("<< List user based on UserId !");
//		
//		return new ResponseEntity<List<Project>>(projectList, HttpStatus.CREATED);
//	}
}
